FROM node:13.6.0 AS base
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
#EXPOSE 4200
RUN npm run build

FROM node:13.6.0
COPY --from=base /usr/src/app server.js
COPY --from=base /usr/src/app/dist dist
CMD [ "node", "server.js" ]
