import { Component, OnInit, Input } from '@angular/core';
import {GeneralUserAndMonument} from '../../examples/dto/GeneralUserAndMonument';
import {HttpClient} from '@angular/common/http';
import {SearchComponent} from '../../examples/search/search.component';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment.prod';

@Component({
  selector: 'app-combo-box',
  templateUrl: './combo-box.component.html',
  styleUrls: ['./combo-box.component.css']
})
export class ComboBoxComponent implements OnInit {

  @Input() list: string[];
  // two way binding for input text
  inputItem = '';
  // enable or disable visiblility of dropdown
  listHidden = true;
  showError = false;
  selectedIndex = -1;

  // the list to be shown after filtering
  filteredList: string[] = [];
  prevList: GeneralUserAndMonument[] = [];

  constructor(private router: Router, public http: HttpClient) { }

  ngOnInit() {

    this.filteredList = this.list;
  }

  // modifies the filtered list as per input
  getFilteredList() {
    this.listHidden = false;
    this.http.get(environment.baseUrl + '/search/subSearch?substring=' + this.inputItem)
        .subscribe((data: GeneralUserAndMonument[]) => this.prevList = data,
            error => console.log('This is called when error occurs'), () => this.cost());
    if (!this.listHidden && this.inputItem !== undefined) {
      this.filteredList = this.list.filter((item) => item.toLowerCase().includes(this.inputItem.toLowerCase()));
    }
  }

  cost() {
    this.list = [];
    for (let i = 0; i < this.prevList.length; i++) {
      if (this.prevList[i].nameMonument !== null) {
        this.list.push(this.prevList[i].nameMonument.replace('\\', '').replace('\\', ''));
      } else {
        this.list.push(this.prevList[i].nickname);
      }
    }
  }

  // select highlighted item when enter is pressed or any item that is clicked
  selectItem(ind) {
    for (let i = 0; i < this.prevList.length ; i++) {
      if (this.filteredList[ind] === this.prevList[i].nickname && this.selectedIndex !== -1) {
        console.log('User');
        location.replace('/profile-user?id=' + this.prevList[i].id);
      }
      if (this.filteredList[ind] === this.prevList[i].nameMonument && this.selectedIndex !== -1) {
        console.log('Monum');
        location.replace('/monument-information/' + this.prevList[i].placeId);
      }
    }
    this.inputItem = this.filteredList[ind];
    this.listHidden = true;
    this.selectedIndex = ind;
  }

  onEnter(inputElement: string) {
    SearchComponent.searchElement = inputElement;
    this.router.navigate(['/search-page']);
  }
  // navigate through the list of items
  onKeyPress(event) {

    if (!this.listHidden) {
      if (event.key === 'Escape') {
        this.selectedIndex = -1;
        this.toggleListDisplay(0);
      }

      if (event.key === 'Enter') {

        this.toggleListDisplay(0);
      }
      if (event.key === 'ArrowDown') {

        this.listHidden = false;
        this.selectedIndex = (this.selectedIndex + 1) % this.filteredList.length;
        if (this.filteredList.length > 0 && !this.listHidden) {
          document.getElementsByTagName('list-item')[this.selectedIndex].scrollIntoView();
        }
      } else if (event.key === 'ArrowUp') {

        this.listHidden = false;
        if (this.selectedIndex <= 0) {
          this.selectedIndex = this.filteredList.length;
        }
        this.selectedIndex = (this.selectedIndex - 1) % this.filteredList.length;

        if (this.filteredList.length > 0 && !this.listHidden) {

          document.getElementsByTagName('list-item')[this.selectedIndex].scrollIntoView();
        }
      }
    }
  }

  // show or hide the dropdown list when input is focused or moves out of focus
  toggleListDisplay(sender: number) {
    if (sender === 1) {
      // this.selectedIndex = -1;
      this.listHidden = false;
      this.getFilteredList();
    } else {
      // helps to select item by clicking
      setTimeout(() => {
        this.selectItem(this.selectedIndex);
        this.listHidden = true;
        if (!this.list.includes(this.inputItem)) {
          this.showError = true;
          this.filteredList = this.list;
        } else {
          this.showError = false;
        }
      }, 500);
    }
  }
}
