import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsComponent } from './components/components.component';
import { ProfileComponent } from './examples/profile/profile.component';
import { SignupComponent } from './examples/signup/signup.component';
import { LandingComponent } from './examples/landing/landing.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import {RouteCreationComponent} from './examples/routeCreation/routeCreation.component';
import {RegistrationComponent} from './examples/registration/registration.component';
import {MessageComponent} from './examples/message/message.component';
import {SearchComponent} from './examples/search/search.component';
import {ProfileNewComponent} from './examples/profile-new/profile-new.component';
import {MapComponent} from './examples/map/map.component';
import {MonumentPageComponent} from './examples/monumentPage/monumentPage.component';
import {HotelPageComponent} from './examples/hotelPage/hotelPage.component';
import {WebsocketChatComponent} from './examples/websocket-chat/websocket-chat.component';
import {UserProfileComponent} from './examples/user-profile/user-profile.component';
import {EmailConfirmComponent} from './examples/emailСonfirm/emailConfirm.component';
import {AfterRegistrationComponent} from './examples/afterRegistration/afterRegistration.component';
import {UserFollowersComponent} from './examples/user-followers/user-followers.component';
import {ShowWayComponent} from './examples/showWay/showWay.component';
import {ModerationComponent} from "./examples/moderation/moderation.component";
import {VkComponent} from "./examples/vk/vk.component";
import {GoogleComponent} from "./examples/google/google.component";
import {FacebookComponent} from "./examples/facebook/facebook.component";
import {VkConnectComponent} from "./examples/vkConnect/vkConnect.component";
import {GoogleConnectComponent} from "./examples/googleConnect/googleConnect.component";
import {FacebookConnectComponent} from "./examples/facebookConnect/facebookConnect.component";
import {UserGeneralComponent} from './examples/user-general/user-general.component';
import {ForgotPasswordComponent} from "./examples/forgotPassword/forgotPassword.component";

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home',             component: ComponentsComponent },
    { path: 'user-profile',     component: ProfileComponent },
    { path: 'login',           component: SignupComponent },
    { path: 'landing',          component: LandingComponent },
    { path: 'nucleoicons',      component: NucleoiconsComponent },
    { path: 'route-creation',   component: RouteCreationComponent},
    { path: 'route-creation/:id',   component: RouteCreationComponent},
    { path: 'registration',     component: RegistrationComponent},
    { path: 'messages',         component: MessageComponent},
    { path: 'search-page',      component: SearchComponent},
    { path: 'profile',          component: ProfileNewComponent},
    { path: 'monument-information/:id',   component: MonumentPageComponent},
    { path: 'hotel-information/:id',   component: HotelPageComponent},
    { path: 'chat', component: WebsocketChatComponent},
    { path: 'profile-user', component: UserProfileComponent},
    { path: 'profile-user/:id', component: UserProfileComponent},
    { path: 'email_confirm',     component: EmailConfirmComponent},
    { path: 'after_registration',     component: AfterRegistrationComponent},
    { path: 'subscriptions', component: UserFollowersComponent},
    { path: 'show-way/:id',         component: ShowWayComponent},
    { path: 'moderation', component: ModerationComponent},
    { path: 'general', component: UserGeneralComponent},
    { path: 'moderation', component: ModerationComponent},
    { path: 'sign-in/oauth/vk',         component: VkComponent},
    { path: 'sign-in/oauth/google',         component: GoogleComponent},
    { path: 'sign-in/oauth/facebook',         component: FacebookComponent},
    { path: 'connect/oauth/vk',         component: VkConnectComponent},
    { path: 'connect/oauth/google',         component: GoogleConnectComponent},
    { path: 'connect/oauth/facebook',         component: FacebookConnectComponent},
    { path: 'forgot_password',         component: ForgotPasswordComponent}
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: false
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
