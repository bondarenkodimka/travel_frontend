import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WayShowComponent } from './way-show.component';

describe('WayShowComponent', () => {
  let component: WayShowComponent;
  let fixture: ComponentFixture<WayShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WayShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WayShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
