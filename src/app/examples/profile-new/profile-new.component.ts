import {Component, Input, NgZone, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {RouteCreationService} from '../routeCreation/routeCreation.service';
import {Status} from '../dto/routeCreation.status';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Place} from '../dto/profile-new.placeCreation';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Cloudinary} from '@cloudinary/angular-5.x';
import {FileItem, FileUploader, FileUploaderOptions, ParsedResponseHeaders} from 'ng2-file-upload';
import {HttpClient} from '@angular/common/http';
import {JsonPipe} from '@angular/common';
import {City} from '../dto/routeCreation.city';
import {Country} from '../dto/routeCreation.country';
import {Monument} from '../dto/routeCreation.monument';

@Component({
    selector: 'app-profile-new',
    templateUrl: './profile-new.component.html',
    styleUrls: ['./profile-new.component.scss'],
    providers: [RouteCreationService]
})
export class ProfileNewComponent implements OnInit {
    latitude = 51.661535;
    longitude = 39.200287;
    locationChosen = false;
    countryTitle = 'Выберите страну';
    city: City[] = [];
    cityObject: City;
    country: Country[] = [];
    statuses: Status[];
    url: string | ArrayBuffer = '../assets/img/image_placeholder.jpg';
    selectCategories = [];
    showCategories = false;
    change = false;
    registerForm: FormGroup;
    submitted = false;
    getObj: Place;
    closeResult: string;
    @Input()
    responses: Array<any>;

    hasBaseDropZoneOver = false;
    uploader: FileUploader;
    uploaderOptions: FileUploaderOptions = {
        url: 'https://api.cloudinary.com/v1_1/daqgh2ufv/image/upload',
        autoUpload: true,
        isHTML5: true,
        removeAfterUpload: false,
        headers: [
            {
                name: 'X-Requested-With',
                value: 'XMLHttpRequest'
            }
        ]
    };

    constructor(private httpService: RouteCreationService, private formBuilder: FormBuilder, private modalService: NgbModal,
                private cloudinary: Cloudinary,
                private zone: NgZone,
                private http: HttpClient) {
        this.responses = [];
    }

    ngOnInit(): void {
        this.httpService.getStatus().subscribe((data: Status[]) => this.statuses = data);
        this.httpService.getCountries().subscribe((data: Country[]) => {
                this.country = data
            },
            error => console.log('This is called when error occurs'), () => this.cost());
        this.registerForm = this.formBuilder.group({
            name: ['', Validators.required]
        });

        this.uploader = new FileUploader(this.uploaderOptions);

        this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
            form.append('upload_preset', 'ml_default');
            form.append('file', fileItem);
            fileItem.withCredentials = false;
            // return {fileItem, form};
        };

        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            const responsePath = JSON.parse(response);
            this.url = responsePath.url;
            console.log(responsePath.url);
        };
    }

    cost() {
        this.sortCountry();
    }

    changeF() {
    }

    viewProfile() {
        location.replace('/profile-user?id=' + localStorage.getItem('id'));
    }
    
    fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    onChoseLocation(event) {
        this.latitude = event.coords.lat;
        this.longitude = event.coords.lng;
        this.locationChosen = true;
    }

    onChange() {
        this.httpService.sendCountry(this.countryTitle).subscribe((newList: City[]) => {
                this.city = newList
            },
            error => console.log('This is called when error occurs'), () => this.cost1());
    }

    cost1() {
        this.sortCity();;
    }

    sortCity() {
        for (let i = 0; i < this.city.length - 1; i++) {
            for (let j = 0; j < this.city.length - 1; j++) {
                if (this.city[j].nameCity > this.city[j + 1].nameCity) {
                    const temp = this.city[j];
                    this.city[j] = this.city[j + 1];
                    this.city[j + 1] = temp;
                }
            }
        }
    }

    sortCountry() {
        for (let i = 0; i < this.country.length - 1; i++) {
            for (let j = 0; j < this.city.length - 1; j++) {
                if (this.country[j].nameCountry > this.country[j + 1].nameCountry) {
                    const temp = this.country[j];
                    this.country[j] = this.country[j + 1];
                    this.country[j + 1] = temp;
                }
            }
        }
    }

    onSelectFile(event) {
        this.change = true;
        if (event.target.files && event.target.files[0]) {
            const reader = new FileReader();

            reader.readAsDataURL(event.target.files[0]); // read file as data url

            reader.onload = (ev) => {
                this.url = reader.result;
            };
        }
    }

    onChangeCity() {
        this.onPaintLocationCity();
    }

    onPaintLocationCity() {
        this.latitude = this.cityObject.latitude;
        this.longitude = this.cityObject.longitude;
        this.locationChosen = true;
    }

    addCategory(name: string) {
        let flag = false;
        for (let i = 0; i < this.selectCategories.length; i++) {
            if (this.selectCategories[i] === name) {
                flag = true;
            }
        }
        if (!flag) {
            this.selectCategories.push(name);
        }
        if (this.selectCategories.length !== 0) {
            this.showCategories = true;
        } else {
            this.showCategories = false;
        }
    }

    deleteCategory(name: string) {
        for (let i = 0; i < this.selectCategories.length; i++) {
            if (this.selectCategories[i] === name) {
                this.selectCategories.splice(i, 1);
            }
        }
        if (this.selectCategories.length === 0) {
            this.showCategories = false;
        }

    }

    get f() {
        return this.registerForm.controls;
    }

    cancel() {
        location.replace('/profile-user');
    }

    onSubmit(content, name: string, description: string, city: string, country: string) {
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        const place: Place = new Place(name, this.longitude, this.latitude, description, this.selectCategories, city, country);
        this.open(content);
        // tslint:disable-next-line:max-line-length
        //return this.httpService.sendInfo(place, this.url.toString()).subscribe((data: Place) => this.getObj = data);
        return this.httpService.sendToModeration(place, this.url.toString()).subscribe((data: Place) => this.getObj = data);
    }

    open(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

}
