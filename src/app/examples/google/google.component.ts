import {Component, OnInit} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {ApiResponse} from "../dto/apiResponse";
import {environment} from '../../../environments/environment.prod';
import {Role} from "../user-profile/role";

@Component({
    selector: 'app-google',
    templateUrl: './google.component.html',
    styleUrls: ['./google.component.scss']
})

export class GoogleComponent implements OnInit {
    code: string;
    constructor(
        private http: HttpClient,
        private route: ActivatedRoute
    ) {}
    ngOnInit() {
        this.route.queryParams.subscribe(
            (queryParam: any) => {
                this.code = queryParam['code'];
            }
        );
        const url = environment.baseUrl + '/sign-in/google';
                const header = new HttpHeaders()
                    .set('code', this.code);
                this.http.get(url, {headers: header}).
                subscribe((data: ApiResponse)  => {
                    if (data.status === 200) {
                        localStorage.setItem('id', data.userInfo.id);
                        localStorage.setItem('token', data.userInfo.token);
                        this.http.get(environment.baseUrl + '/user/role?id=' + localStorage.getItem('id')).subscribe((data: Role) => {localStorage.setItem('role', data.role);
                            location.replace(environment.uiUrl + '/home');
                        });

                    } else {
                        location.replace(environment.uiUrl + '/home');
                    }
                });
    }
}
