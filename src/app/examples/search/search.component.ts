import {Component, ElementRef, OnInit} from '@angular/core';
import {SearchService} from './search.service';
import {Monument} from '../dto/routeCreation.monument';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {Country} from '../dto/routeCreation.country';
import {City} from '../dto/routeCreation.city';
import {MapComponent} from '../map/map.component';
import {Status} from '../dto/routeCreation.status';
import {User} from '../dto/routeCreation.user';
import {Router} from '@angular/router';
import {MonumentPageComponent} from '../monumentPage/monumentPage.component';
import {Location} from '@angular/common';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    providers: [SearchService]
})
export class SearchComponent implements OnInit {
    static searchElement = '';
    copySearch = SearchComponent.searchElement;
    monuments: PhotoMonument[] = [];
    isCollapsed1 = false;
    isCollapsed2 = false;
    country: Country[] = [];
    city: City[] = [];
    monument: Monument[] = [];
    countryTitle = 'Выберите страну';
    cityObject: City = null;
    map: MapComponent;
    wait = false;
    typeMonument: Status[] = [];
    statuses: Status[] = [];
    users: User = null;
    prov = false;
    prov1 = true;
    found = false;
    open = false;


    constructor(private httpService: SearchService, public location: Location) {
    }

    ngOnInit(): void {
        this.users = null;
        this.monuments = [];
        this.httpService.getCountries().subscribe((data: Country[]) => this.country = data);
        this.httpService.getStatus().subscribe((data: Status[]) => this.statuses = data);
        // this.httpService.getCity().subscribe((data: City[]) => this.city = data);
        if (SearchComponent.searchElement !== '') {
            this.wait = true;
            // tslint:disable-next-line:max-line-length
            this.httpService.getByNameMonument(SearchComponent.searchElement).subscribe((newList: PhotoMonument[]) => this.monuments = newList, error => console.log('This is called when error occurs'), () => this.workMonument());
            this.httpService.getUser(SearchComponent.searchElement).subscribe((data: User) => this.users = data, error => console.log('This is called when error occurs'), () => this.workUser());
        }
    }

    getMonuments(name: string) {
        this.users = null;
        this.monuments = [];
        this.wait = true;
        this.httpService.getMonumentsByCity(name).subscribe((data: PhotoMonument[]) => this.monuments = data,
            error => console.log('This is called when error occurs'), () => this.newMeth());

    }

    newMeth() {
        this.prov = true;
        this.wait = false;
    }

    onFilter() {
        this.users = null;
        this.monuments = [];
        for (let i = 0; i < this.statuses.length; i++) {
            // tslint:disable-next-line:curly
            if (this.statuses[i].checked === true) {
                this.typeMonument.push(this.statuses[i]);
            }
        }
        if (this.typeMonument.length === 0) {
            this.wait = false;
        } else {
            this.wait = true;
        }
        if (this.typeMonument.length !== 0) {
            this.wait = true;
            if (this.cityObject === null) {
                // tslint:disable-next-line:max-line-length
                this.httpService.filterMonuments(this.typeMonument)
                    .subscribe((data: PhotoMonument[]) => this.monuments = data, error => console.log('This is called when error occurs'), () => this.cost1());
            } else {
                this.httpService.filterMonumentsNew(this.typeMonument, this.cityObject.nameCity)
                // tslint:disable-next-line:max-line-length
                    .subscribe((data: PhotoMonument[]) => this.monuments = data, error => console.log('This is called when error occurs'), () => this.cost1());
            }
        }
        this.typeMonument = [];
    }

    onUser(name: string) {
        this.found = false;
        this.open = false;
        this.display();
        this.users = undefined;
        this.monuments = [];
        this.prov = false;
        this.copySearch = '';
        this.wait = true;
        // tslint:disable-next-line:max-line-length
        this.httpService.getUser(name).subscribe((data: User) => this.users = data, error => console.log('This is called when error occurs'), () => this.checkUser());
    }

    checkUser() {
        this.wait = false;
        if (this.users !== undefined && this.users !== null) {
            this.prov1 = false;
        } else if (this.users === null) {
            this.copySearch = '';
            this.found = true;
        }
    }

    routing(placeId: string) {
        MonumentPageComponent.placeId = placeId;
    }

    onChange() {
        this.prov = false;
        this.prov1 = true;
        this.users = null;
        this.monuments = [];
        this.httpService.sendCountry(this.countryTitle).subscribe((newList: City[]) => this.city = newList);
        this.httpService.getMonumentsByCountry(this.countryTitle).subscribe((data: PhotoMonument[]) => this.monuments = data,
            error => console.log('This is called when error occurs'), () => this.cost1());
    }

    onChangeCity() {
        this.httpService.sendCity(this.cityObject.nameCity).subscribe((newList: PhotoMonument[]) => this.monuments = newList);
    }

    getByNameMonument(name: string) {
        this.found = false;
        this.open = true;
        this.prov1 = true;
        this.users = null;
        this.monuments = [];
        if (name !== '') {
            this.copySearch = '';
            this.display();
            this.wait = true;
            // tslint:disable-next-line:max-line-length
            this.httpService.getByNameMonument(name).subscribe((newList: PhotoMonument[]) => this.monuments = newList, error => console.log('This is called when error occurs'), () => this.workMonument1(name));
            // this.httpService.getUser(name).subscribe((data: User) => this.users = data, error => console.log('This is called when error occurs'), () => this.workUser());
        }
    }

    nameMonument() {
        if (this.monuments.length === 0) {
            this.found = true;
            this.copySearch = '';
        }
    }

    cost1() {
        this.wait = false;
        this.prov = true;
    }

    display() {
        this.prov = true;
    }

    workMonument() {
        this.wait = false;
        if (this.monuments !== [] && this.monuments !== null) {
            this.found = false;
            this.prov = true;
            this.prov1 = true;
        } else {
            this.found = true;
        }
    }

    workMonument1(name: string) {
        this.wait = false;
        if (this.monuments !== [] && this.monuments !== null) {
            this.found = false;
            this.prov = true;
            this.prov1 = true;
        } else {
            // tslint:disable-next-line:max-line-length
            this.httpService.getUser(name).subscribe((data: User) => this.users = data, error => console.log('This is called when error occurs'), () => this.workUser());
            // this.found = true;
        }
           }

    workUser() {
        this.wait = false;
        if (this.users !== null) {
            console.log(this.users);
            this.found = false;
            this.prov1 = false;
            this.prov = false;
        } else if (this.monuments.length === 0 && this.users === null) {
            this.found = true;
        }
    }

    viewProfile(id: string) {
        location.replace('/profile-user?id=' + id);
    }
}
