import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {Country} from '../dto/routeCreation.country';
import {City} from '../dto/routeCreation.city';
import {Monument} from '../dto/routeCreation.monument';
import {Status} from '../dto/routeCreation.status';
import {User} from '../dto/routeCreation.user';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment.prod';
import {SearchComponent} from './search.component';

@Injectable()
export class SearchService {
    constructor(private http: HttpClient) {
    }

    getMonumentsByCity(city: string) {
        return this.http.get<PhotoMonument[]>(environment.baseUrl + '/search/monuments/' + city);
    }

    getCountries() {
        return this.http.get<Country[]>(environment.baseUrl + '/countries');
    }

    getMonumentsByCountry(name: string) {
        return this.http.get<PhotoMonument[]>(environment.baseUrl + '/countries/' + name);
    }

    sendCountry(country: string) {
        return this.http.get<City[]>(environment.baseUrl + '/cities/' + country);
    }

    getCity() {
        return this.http.get<City[]>(environment.baseUrl + '/cities');
    }

    sendCity(city: string) {
        return this.http.get<PhotoMonument[]>(environment.baseUrl + '/monuments/' + city);
    }

    getUser(name: string) {
        return this.http.get<User>(environment.baseUrl + '/search/user/' + name);
    }

    getByNameMonument(name: string) {
        return this.http.get<PhotoMonument[]>(environment.baseUrl + '/search/monument/' + name)
    }
    filterMonuments(typeMonument: Status[]) {
        let url = '';
        for (let i = 0; i < typeMonument.length - 1; i++) {
            url = url + typeMonument[i].nameStatus + ',';
        }
        url = url + typeMonument[typeMonument.length - 1].nameStatus;
        return this.http.get<PhotoMonument[]>(environment.baseUrl + '/monuments/types?types=' + url);
    }

    filterMonumentsNew(typeMonument: Status[], name: string) {
        let url = '';
        for (let i = 0; i < typeMonument.length - 1; i++) {
            url = url + typeMonument[i].nameStatus + ',';
        }
        url = url + typeMonument[typeMonument.length - 1].nameStatus;
        return this.http.get<PhotoMonument[]>(environment.baseUrl + '/search/' + name + '/types?types=' + url);
    }

    getStatus() {
        return this.http.get<Status[]>(environment.baseUrl + '/statuses');
    }
}
