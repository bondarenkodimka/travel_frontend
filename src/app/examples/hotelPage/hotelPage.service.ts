import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AllPhotoHotel} from './dto/hotelPage.photo';
import {environment} from '../../../environments/environment.prod';

@Injectable()
export class HotelPageService {
    constructor(private http: HttpClient) {}

    getAllPhotos(placeId: string) {
        return this.http.get<AllPhotoHotel>(environment.baseUrl + '/hotel/photos/' + placeId);
    }

}
