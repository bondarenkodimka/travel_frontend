export class City {
    longitude: number;
    latitude: number;
    // tslint:disable-next-line:variable-name
    nameCity: string;
}
