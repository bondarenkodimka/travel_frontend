export class FilterHotel {
    cityHotel: string;
    minPriceHotel: number;
    maxPriceHotel: number;
    minRatingHotel: number;
    maxRatingHotel: number;
    star: number;
}
