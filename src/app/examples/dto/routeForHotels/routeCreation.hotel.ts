export class Hotel {
    longitude: number;
    latitude: number;
    nameHotel: string;
    priceAvg: number;
    avgPrice: number;
    stars: number;
    rating: number;
    site: string;
    formattedAddress: string;
    formattedPhoneNumber: string;
    placeId: string;
    photo: string;
}
