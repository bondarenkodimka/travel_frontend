export class HotelPageResponse {
    idUser: string;
    userNickname: string;
    photoUser: string;
    photo: string[];
    text: string;
    idComment: string;
    date: Date;
    check: boolean;
}
