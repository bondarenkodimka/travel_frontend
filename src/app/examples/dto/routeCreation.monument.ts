export class Monument {
    nameMonument: string;
    longitude: number;
    latitude: number;
    placeId: string;
    link: string;
    checked: boolean;
}
