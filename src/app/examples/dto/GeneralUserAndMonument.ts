import {WorkTimeMonument} from './routeCreation.workTimeMonument';

export class GeneralUserAndMonument {
    nameMonument: string;
    photoMonument: string;
    placeId: string;
    monumentInfo: string;
    latitude: number;
    longitude: number;
    workTimeMonument: WorkTimeMonument[];
    nameStatus: string;
    id: string;
    login: string;
    nickname: string;
    role: string;
    photo: string;
}
