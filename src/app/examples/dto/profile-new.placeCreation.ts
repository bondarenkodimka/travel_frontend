export class Place {
    private _name: string;
    private _longitude: number;
    private _latitude: number;
    private _description: string;
    private _category: string[];
    private _city: string;
    private _country: string;


    constructor(name: string, longitude: number, latitude: number, description: string, category: string[], city: string, country: string) {
        this._name = name;
        this._longitude = longitude;
        this._latitude = latitude;
        this._description = description;
        this._category = category;
        this._city = city;
        this._country = country;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get longitude(): number {
        return this._longitude;
    }

    set longitude(value: number) {
        this._longitude = value;
    }

    get latitude(): number {
        return this._latitude;
    }

    set latitude(value: number) {
        this._latitude = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get city(): string {
        return this._city;
    }

    set city(value: string) {
        this._city = value;
    }

    get country(): string {
        return this._country;
    }

    set country(value: string) {
        this._country = value;
    }

    get category(): string[] {
        return this._category;
    }

    set category(value: string[]) {
        this._category = value;
    }
}
