import {CheckPointResponses} from './CheckPointResponses';
import {InformationAboutMonument} from './InformationAboutMonument';

export class TripForLenta {
    monuments: InformationAboutMonument[];
    nameTrip: string;
    id: string;
    status: boolean;
}
