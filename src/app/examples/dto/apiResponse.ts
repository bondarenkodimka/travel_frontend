import {AuthToken} from "./authToken";


export interface ApiResponse {
    status: number;
    message: string;
    userInfo: AuthToken;
}
