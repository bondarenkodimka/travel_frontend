import {Monument} from './routeCreation.monument';
import {PhotoMonument} from './routeCreation.photoMonument';

export class TripMonumentResponse {
    monument: PhotoMonument;
    numberInWay: number;
    visit: boolean;
}
