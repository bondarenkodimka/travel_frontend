import {Steps} from './routeCreation.steps';

export class Trip {
    private _start: string;
    private _end: string;
    private _distance: string;
    private _duration: string;
    private _steps: Steps[] = [];


    get start(): string {
        return this._start;
    }

    set start(value: string) {
        this._start = value;
    }

    get end(): string {
        return this._end;
    }

    set end(value: string) {
        this._end = value;
    }

    get distance(): string {
        return this._distance;
    }

    set distance(value: string) {
        this._distance = value;
    }

    get duration(): string {
        return this._duration;
    }

    set duration(value: string) {
        this._duration = value;
    }

    get steps(): Steps[] {
        return this._steps;
    }

    set steps(value: Steps[]) {
        this._steps = value;
    }
}
