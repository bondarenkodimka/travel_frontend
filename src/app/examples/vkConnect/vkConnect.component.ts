import {Component, OnInit} from "@angular/core";
import {ApiResponse} from "../dto/apiResponse";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {environment} from '../../../environments/environment.prod';

@Component({
    selector: 'app-vk',
    templateUrl: './vkConnect.component.html',
    styleUrls: ['./vkConnect.component.scss']
})

export class VkConnectComponent implements OnInit {
    code: string;
    constructor(
        private http: HttpClient,
        private route: ActivatedRoute
    ) {}
    ngOnInit() {
        this.route.queryParams.subscribe(
            (queryParam: any) => {
                this.code = queryParam['code'];
            }
        );
        const url = environment.baseUrl + '/connect/vk';
        const header = new HttpHeaders()
            .set('code', this.code)
            .set('Id', localStorage.getItem('id'))
            .set('Token', localStorage.getItem('token'));
        this.http.get(url, {headers: header}).
        subscribe((data: ApiResponse)  => {
            if (data.status === 200) {
                location.replace(environment.uiUrl + '/profile-user');
            } else {
                alert(data.message);
                location.replace(environment.uiUrl + '/profile-user');
            }
        });
    }
}
