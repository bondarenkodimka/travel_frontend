export class General {
    nickname: string;
    name: string;
    surname: string;
    googleConnect: boolean;
    facebookConnect: boolean;
    vkConnect: boolean;
}
