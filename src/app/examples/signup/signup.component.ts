import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiResponse} from "../dto/apiResponse";
import {Role} from "../user-profile/role";
import {environment} from "../../../environments/environment";


@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})

export class SignupComponent implements OnInit {
    test : Date = new Date();
    focus;
    focus1;
    data: ApiResponse;
    registerForm: FormGroup;
    submitted = false;

    constructor(
        private http: HttpClient,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            login: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
            password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9-_]+$')]]
        });
    }

    get f() { return this.registerForm.controls; }

    googleSignIn() {
        location.replace('https://accounts.google.com/o/oauth2/v2/auth?redirect_uri='+ environment.uiUrl +'/sign-in/oauth/google&' +
            'response_type=code&client_id=1004277437940-lj9j1fnb5ml8fahqu75e6rkqel4avgu8.apps.googleusercontent.com&' +
            'scope=https://www.googleapis.com/auth/userinfo.profile');
    }

    vkSignIn() {
        location.replace('https://oauth.vk.com/authorize?client_id=7427646&display=page&' +
            'redirect_uri='+ environment.uiUrl +'/sign-in/oauth/vk&scope=photos&response_type=code&v=5.52');
    }

    facebookSignIn() {
        location.replace('https://www.facebook.com/v6.0/dialog/oauth?client_id=1053017901734123&' +
            'redirect_uri='+ environment.uiUrl +'/sign-in/oauth/facebook&scope=public_profile');
    }

    onSubmit() {
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        const url = environment.baseUrl + '/login';
        const headers = new HttpHeaders()
            .set('login', this.f.login.value)
            .set('password', this.f.password.value);
        this.http.get(url, {headers: headers})
            .subscribe((data: ApiResponse)  => {
                this.data = data;
                if (this.data.status !== 424) {
                    if (this.data.status === 200) {
                        localStorage.setItem('id', this.data.userInfo.id);
                        localStorage.setItem('token', this.data.userInfo.token);
                        this.http.get(environment.baseUrl + '/user/role?id=' + localStorage.getItem('id'))
                            .subscribe((data1: Role) => {localStorage.setItem('role', data1.role);
                        location.replace(environment.uiUrl + '/home')});
                    } else {
                        alert(this.data.message);
                    }
                } else {
                    alert(this.data.message);
                }
            });
    }
}
