import {Component, OnInit} from "@angular/core";
import {ApiResponse} from "../dto/apiResponse";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Component({
    selector: 'app-forgot',
    templateUrl: './forgotPassword.component.html',
    styleUrls: ['./forgotPassword.component.scss']
})

export class ForgotPasswordComponent implements OnInit {
    test: Date = new Date();
    focus;
    focus1;
    data: ApiResponse;
    registerForm: FormGroup;
    submitted = false;

    constructor(
        private http: HttpClient,
        private formBuilder: FormBuilder
    ) {
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            login: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]]
        });
    }

    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        const url = environment.baseUrl + '/password-recovery';
        const headers = new HttpHeaders()
            .set('login', this.f.login.value);
        this.http.get(url, {headers: headers})
            .subscribe((data: ApiResponse)  => {
                this.data = data;
                if (this.data.status === 200) {
                    alert(this.data.message);
                    location.replace(environment.uiUrl + '/signup');
                } else {
                    alert(this.data.message);
                }
            });
    }
}
