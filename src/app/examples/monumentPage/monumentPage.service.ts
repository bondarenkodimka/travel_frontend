import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AllPhotoMonument} from './dto/monumentPage.photo';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {AllPhotoHotel} from '../hotelPage/dto/hotelPage.photo';
import {environment} from '../../../environments/environment.prod';

@Injectable()
export class MonumentPageService {
    constructor(private http: HttpClient) {}

    getAllPhotos(placeId: string) {
        return this.http.get<AllPhotoMonument>(environment.baseUrl + '/photos/' + placeId);
    }

    getAllPhotosByMonument(placeId: string) {
        return this.http.get<AllPhotoMonument>(environment.baseUrl + '/photos/id/' + placeId);
    }

    getMonumentInfo(placeId: string) {
        return this.http.get<PhotoMonument[]>(environment.baseUrl + '/search/monuments/info/' + placeId);
    }

    getWishSearch(placeId: string) {
        return this.http.get<boolean>(environment.baseUrl + '/wish/search?userid=' + localStorage.getItem('id') + '&idPlaceMonument=' + placeId);
    }
    getWishDel(placeId: string) {
        return this.http.get<boolean>(environment.baseUrl + '/wish/delete?userid=' + localStorage.getItem('id') + '&idPlaceMonument=' + placeId);
    }
    addWish(placeId: string) {
        return this.http.get<void>(environment.baseUrl + '/wish/add?userid=' + localStorage.getItem('id') + '&idPlaceMonument=' + placeId);
    }
    addComment(idUser: string, idMonument: string, comment: string, photo: string[], date: Date) {
        const body = {
            idUser: idUser,
            idMonument: idMonument,
            comment: comment,
            photo: photo,
            date: date
        };
        return this.http.post(environment.baseUrl + '/comments/monument', body);
    }

    getComment(id: string) {
        return this.http.get(environment.baseUrl + '/comments?idMonument=' + id);
    }

    deleteComment(idComment: string) {
        return this.http.delete(environment.baseUrl + '/comments/' + idComment);
    }

    getAllPhotosHotel(placeId: string) {
        return this.http.get<AllPhotoHotel>(environment.baseUrl + '/hotel/photos/' + placeId);
    }

    addCommentHotel(idUser: string, idHotel: string, comment: string, photo: string[], date: Date) {
        const body = {
            idUser: idUser,
            idHotel: idHotel,
            comment: comment,
            photo: photo,
            date: date
        };
        return this.http.post(environment.baseUrl + '/comments/hotels', body);
    }

    getCommentHotel(id: string) {
        return this.http.get(environment.baseUrl + '/comments/id?idHotel=' + id);
    }

    deleteCommentHotel(idComment: string) {
        return this.http.delete(environment.baseUrl + '/comments/id/' + idComment);
    }
}
