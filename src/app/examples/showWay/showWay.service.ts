import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Routes} from '../user-profile/user-profile.routes';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {Monument} from '../dto/routeCreation.monument';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {CheckPointResponses} from '../dto/CheckPointResponses';
import {TripUser} from '../dto/tripUser';
import {environment} from '../../../environments/environment.prod';

@Injectable()
export class ShowWayService {
    constructor(private http: HttpClient) {}

    private subscription: Subscription;
    httpOptions = {
        headers: new HttpHeaders({
            'Id': localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    };

    getTrip(id: string) {
        return this.http.get<TripUser>(environment.baseUrl + '/trip/show/' + id);
    }



}
