import {Component, OnInit} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Place} from "../dto/profile-new.placeCreation";
import {UserPlace} from "./userPlace";
import {User} from "../dto/routeCreation.user";
import {environment} from '../../../environments/environment.prod';

@Component({
    selector: 'app-moderation',
    templateUrl: './moderation.component.html',
    styleUrls: ['./moderation.component.scss'],
})
export class ModerationComponent implements OnInit {
    places: UserPlace[] = [];
    latitude: number;
    longitude: number;
    httpOptions = {
        headers: new HttpHeaders({
            'Id': localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    };
    constructor(private http: HttpClient) {
    }
    ngOnInit(): void {
        if(localStorage.getItem('role') == null || localStorage.getItem('role') == 'user'){
            alert('This page is available only for administrator and moderators!');
            location.replace('/home');
        }
        this.loadUserMonums()
        console.log(localStorage.getItem('id'));
        console.log(localStorage.getItem('token'))
    }

    acceptMonument(place: UserPlace) {
        const body = {
            id: place.idMonument
        }
        this.http.put(environment.baseUrl + '/moders?id=' + body.id, null, this.httpOptions).subscribe(() => location.reload());
    }

    declineMonument(place: UserPlace) {
        const body = {
            id: place.idMonument
        }
        this.http.delete(environment.baseUrl + '/moders?id=' + body.id, this.httpOptions).subscribe(() => location.reload());
    }

    loadUserMonums() {
        this.http.get(environment.baseUrl + '/moders', this.httpOptions).subscribe((data: UserPlace[]) => this.places = data)
    }
}
