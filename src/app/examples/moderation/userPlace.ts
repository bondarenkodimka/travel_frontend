export class UserPlace {
    idMonument: string;
    cityMonument: string;
    nameMonument: string;
    longitude: number;
    latitude: number;
    status: string;
    informationAboutMonument: string;
    photo: string;
}