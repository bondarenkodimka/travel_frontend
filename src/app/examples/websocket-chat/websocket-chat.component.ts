import {Component, OnInit} from '@angular/core';
import {AppComponent} from '../../app.component';
import {WebSocketAPI} from './WebSocketApi';
import {WebsocketChatMessage} from './websocket-chat.message';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Dialog} from './dialog';
import {WebsocketChatService} from './websocket-chat.service';
import {createLogErrorHandler} from '@angular/compiler-cli/ngcc/src/execution/tasks/completion';
import {environment} from '../../../environments/environment.prod';


@Component({
    selector: 'app-websocket-chat',
    templateUrl: './websocket-chat.component.html',
    styleUrls: ['./websocket-chat.component.scss'],
})
export class WebsocketChatComponent implements OnInit {
    storage: Storage;
    greeting: any;
    name: string;
    recipient: string;
    previous = '';
    dialogs: Dialog[];
    currentDiag: number;
    webSocketAPI: WebSocketAPI;
    message: WebsocketChatMessage;
    messages: WebsocketChatMessage[];
    httpOptions = {
        headers: new HttpHeaders({
            'Id': localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    }

    constructor(private http: HttpClient) {
    }

    ngOnInit() {

        this.currentDiag = 0;
        this.webSocketAPI = new WebSocketAPI(this);
        this.storage = window.localStorage;
        this.message = new WebsocketChatMessage();
        this.connect();
        if (location.href.includes('?hello=')) {
            console.log('hello')
            this.message.date = Date.now();
            this.message.content = 'hello';
            this.message.sender = localStorage.getItem('id')
            this.message.recipient = location.href.substring(location.href.indexOf('?hello=') + 7, location.href.length);
            setTimeout(() => {
                this.webSocketAPI._send(this.message)
            }, 1000);
            this.messages.push(this.message);
            let i: number;
            i = 0;
            for (i; i < this.dialogs.length; i++) {
                if (this.dialogs[i].senderId === this.message.recipient) {
                    break;
                }
                if (i === (this.dialogs.length - 1)) {
                    // tslint:disable-next-line:prefer-const
                    let dig: Dialog;
                    dig.senderId = this.message.sender;
                    dig.recevierId = this.message.recipient;
                    dig.lastmsg = this.message.content;
                    this.dialogs.push(dig);
                }
            }
            location.href = environment.uiUrl + '/chat'
            this.webSocketAPI._send(this.message)
        }
        console.log(this.storage.getItem('id'))
        this.getDialogs();
        setTimeout(()=> this.changeDiag(0), 250);
    }

    changeDiag(num: number) {
        if (this.previous !== '') {
            document.getElementById(this.previous).style.backgroundColor = '#ffffff';
            document.getElementById(this.previous).style.textDecorationColor = '#F5593D';
        }
        this.previous = num.toString();
        document.getElementById(num.toString()).style.backgroundColor = '#F5593D';
        document.getElementById(this.previous).style.textDecorationColor = '#ffffff';
        this.currentDiag = num;
        // tslint:disable-next-line:max-line-length
        this.http.get(environment.baseUrl + '/messages/?sender=' + this.storage.getItem('id') + '&recent=' + (localStorage.getItem('id') !== this.dialogs[this.currentDiag].recevierId ? this.dialogs[this.currentDiag].recevierId : this.dialogs[this.currentDiag].senderId), this.httpOptions)
            .subscribe((data: WebsocketChatMessage []) => {this.messages = data; var obj = document.getElementById('mes');
                setTimeout(()=> obj.scrollTop=obj.scrollHeight, 50)})
    }


    getMessage(receiver: string) {
        this.http.get(environment.baseUrl + '/messages/?sender=' + this.storage.getItem('id') + '&recent=' + receiver, this.httpOptions)
            .subscribe((data: WebsocketChatMessage []) => this.messages = data)
    }


    getDialogs() {
        this.http.get(environment.baseUrl + '/messages/dialogs/?id=' + this.storage.getItem('id'), this.httpOptions)
            .subscribe((data: Dialog[]) => {
                this.dialogs = data;
                if(location.href.includes('?id=')){
                    let userId: string;
                    userId = location.href.substring(location.href.indexOf('?id=') + 4, location.href.length);
                    location.href = '/chat'
                    for(let i = 0; i < data.length; i++){
                        if(this.dialogs[i].recevierId == userId || this.dialogs[i].senderId == userId){
                            console.log(this.dialogs[i].recevierName)
                            this.changeDiag(i);
                        }
                    }
                }
                else {
                    this.changeDiag(0);
                }
                // this.recipient = this.dialogs[0].recevierId;
                // this.message.recipient = this.recipient
                // console.log('complete')
            })

    }

    connect() {
        this.webSocketAPI._connect();
    }

    disconnect() {
        this.webSocketAPI._disconnect();
    }

    sendMessage() {
        this.message.date = Date.now();
        this.message.content = (<HTMLInputElement>document.getElementById('content')).value;
        this.message.sender = localStorage.getItem('id')
        // tslint:disable-next-line:max-line-length
        this.message.recipient = (localStorage.getItem('id') !== this.dialogs[this.currentDiag].recevierId ? this.dialogs[this.currentDiag].recevierId : this.dialogs[this.currentDiag].senderId);
        this.webSocketAPI._send(this.message);
        (<HTMLInputElement>document.getElementById('content')).value = '';
    }

    handleMessage(message) {
        this.greeting = message;
    }

}
