import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment.prod';

@Injectable()
export class WebsocketChatService {
    httpOptions = {
        headers: new HttpHeaders({
            'Id':  localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    }
    constructor(public http: HttpClient) {}

    messages(sender: string, receiver: string) {
        return this.http.get(environment.baseUrl + '/messages/?sender=' + sender + '&recent=' + receiver, this.httpOptions);
    }
    dialogs(id: string) {
        return this.http.get(environment.baseUrl + '/messages/dialogs/?id=' + id), this.httpOptions;
    }
}
