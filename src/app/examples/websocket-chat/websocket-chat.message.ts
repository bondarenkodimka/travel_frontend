export class WebsocketChatMessage {
    sender: string;
    recipient: string;
    content: string;
    date: number;
}