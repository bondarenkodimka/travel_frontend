import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from "@angular/router";
import {ApiResponse} from "../dto/apiResponse";
import {environment} from '../../../environments/environment.prod';

@Component({
    selector: 'app-emailing',
    templateUrl: './emailConfirm.component.html',
    styleUrls: ['./emailConfirm.component.scss']
})

export class EmailConfirmComponent implements OnInit {
    emailActivation: string;
    data: ApiResponse;
    messageServer: string;
    message: string;
    constructor(
        private http: HttpClient,
        private route: ActivatedRoute
        ) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe(
            (queryParam: any) => {
                this.emailActivation = queryParam['emailActivation'];
            }
        );
        this.http.get(environment.baseUrl + '/email_confirm?emailActivation=' + this.emailActivation).subscribe((data: ApiResponse)  => {
            this.data = data;
            this.messageServer = this.data.message;
            if (this.data.status === 200) {
                this.message = 'You will be redirected to the main page.';
                setTimeout(() => {
                    localStorage.setItem('id', this.data.userInfo.id);
                    localStorage.setItem('token', this.data.userInfo.token);
                    location.replace(environment.uiUrl + '/home');
                }, 2000)
            } else {
                if (this.data.status === 208) {
                    this.message = 'You will be redirected to the sign up page.';
                    setTimeout(() => {
                        location.replace(environment.uiUrl + '/signup');
                    }, 2000)
                } else {
                    this.message = 'Account has been deleted or blocked.';
                    setTimeout(() => {
                        location.replace(environment.uiUrl + '/registration');
                    }, 2000)
                }
            }
        });
    }
}
