import {Component, OnInit} from "@angular/core";
import {General} from "../user-profile/general";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from '../../../environments/environment.prod';
import {ApiResponse} from "../dto/apiResponse";

@Component({
    selector: 'app-profile-new',
    templateUrl: './user-general.component.html',
    styleUrls: ['./user-general.component.scss'],
})
export class UserGeneralComponent implements OnInit{
    id: string;
    idnum: number;
    general: General;
    storage: Storage;
    exists: boolean;
    passwordMessage: string;
    httpOptions = {
        headers: new HttpHeaders({
            'Id': localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    };
    constructor(private http: HttpClient) {
    }
    ngOnInit(): void {
        this.exists = false;
        this.passwordMessage = '';
        this.storage = localStorage;
        if(!location.href.includes('?id=')){
            this.id = localStorage.getItem('id');
            location.href += ('?id=' + this.id);
        } else {
            this.idnum = location.href.indexOf('?id=');
            this.id = location.href.substring(this.idnum + 4, location.href.length);
        }
        this.getUserInfo();
    }

    acceptInfo() {
        const body= {
            id: this.id,
            name: (<HTMLInputElement>document.getElementById('name')).value,
            surname: (<HTMLInputElement>document.getElementById('surname')).value,
            nickname: (<HTMLInputElement>document.getElementById('nickname')).value
        }
        if(this.general.nickname==body.nickname){
            body.nickname='';
        }
        if((<HTMLInputElement>document.getElementById('oldpass')).value != "") {
            if((<HTMLInputElement>document.getElementById('newpass')).value != "" && (<HTMLInputElement>document.getElementById('newpass')).value.length >= 8) {
                const passBody = {
                    id: this.id,
                    oldpass: (<HTMLInputElement>document.getElementById('oldpass')).value,
                    newpass: (<HTMLInputElement>document.getElementById('newpass')).value
                }
                this.http.put(environment.baseUrl + '/user/password?id=' + passBody.id +
                '&oldpass=' + passBody.oldpass + '&newpass=' + passBody.newpass, null, this.httpOptions).subscribe(
                    (data:ApiResponse) => {
                        if(data.status!=200) {
                            this.passwordMessage = data.message;
                        }
                        else {
                            this.passwordMessage = data.message;
                        }
                        (<HTMLInputElement>document.getElementById('oldpass')).value = "";
                        (<HTMLInputElement>document.getElementById('newpass')).value = "";
                    }
                )
            }
        }
        this.http.put(environment.baseUrl + '/user/newgeneral?id=' + body.id + '&name=' +
            body.name + '&surname=' + body.surname + '&nickname=' + body.nickname,null, this.httpOptions).subscribe(
                (data: ApiResponse)=> {
                    if(data.status!=200) {
                        this.exists = true;
                    } else {
                        this.exists = false;
                        location.reload();
                    }
                }
        );
    }

    changePass() {

    }

    declineChanges() {
        location.replace(environment.uiUrl + '/profile-user/');
    }

    getUserInfo() {
        // tslint:disable-next-line:max-line-length
        this.http.get(environment.baseUrl + '/user/general/?id=' + this.id).subscribe((nick: General) => this.general = nick);
    }
}
