export const environment = {
  production: true,
  baseUrl: 'https://dream-journey-server.herokuapp.com',
  uiUrl: 'https://dream-journey-website.herokuapp.com'
};

